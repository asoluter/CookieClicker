package com.asoluter.cookieclicker;

import android.app.ActionBar;
import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.CountDownTimer;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.math.BigInteger;
import java.util.Timer;
import java.util.TimerTask;


public class MainActivity extends Activity {

    ImageButton imageButton,sShop;
    Animation animation;
    TextView cookieCount,cookieSecond;
    CountDownTimer countDownTimer;

    LinearLayout fShop;
    Button clean,bShop;
    BigInteger ccount,cclick,csecond;
    SharedPreferences preferences;
    SharedPreferences.Editor editor;

    public static final String APP_PREFERENCES="mysettings";
    public static final String C_COUNT="C_COUNT";
    public static final String C_CLICK="C_CLICK";
    public static final String C_SECOND="C_SECOND";
    public Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        //ACTIVITY_NEEDED

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //END_OF_ACTIVITY_NEEDED



        cookieSecond=(TextView)findViewById(R.id.perSecond);
        cookieCount=(TextView)findViewById(R.id.cookieCount);
        animation= AnimationUtils.loadAnimation(this, R.anim.cookie_size);
        imageButton=(ImageButton)findViewById(R.id.cookie);
        imageButton.setOnClickListener(onClickListener);
        clean=(Button)findViewById(R.id.clean);
        clean.setOnClickListener(onClickListener);

        sShop=(ImageButton)findViewById(R.id.sShop);
        sShop.setOnClickListener(onClickListener);
        bShop=(Button)findViewById(R.id.bShop);
        bShop.setOnClickListener(onClickListener);
        fShop=(LinearLayout)findViewById(R.id.frameShop);
        fShop.setVisibility(View.GONE);



        //FONT_SETTINGS
        font=Typeface.createFromAsset(getAssets(),getString(R.string.cookie_font));

        cookieCount.setTypeface(font);
        cookieSecond.setTypeface(font);

        //END_FONT_SETTINGS



        //COOKIE_TIMER

        countDownTimer=new CountDownTimer(1000,1000) {
            @Override
            public void onTick(long l) {
                ccount=ccount.add(csecond);
                cookieCount.setText(ccount.toString()+" cookies");
                editor.putString(C_COUNT, ccount.toString());
                editor.apply();
            }

            @Override
            public void onFinish() {
                countDownTimer.start();
            }
        };

        //END_OF_COOKIE_TIMER



        //PREFERENCES_MAIN

        preferences=getSharedPreferences(APP_PREFERENCES,MODE_PRIVATE);
        editor=preferences.edit();
        String s=preferences.getString(C_COUNT, "");
        if(s.equals(""))
        {
            editor.putString(C_COUNT,"0");
            editor.putString(C_CLICK,"1");
            editor.putString(C_SECOND,"0");
            editor.apply(); s="0";
        }
        ccount=new BigInteger(s);
        cookieCount.setText(s+" cookies");

        s=preferences.getString(C_CLICK, "");
        cclick=new BigInteger(s);

        s=preferences.getString(C_SECOND,"");
        csecond=new BigInteger(s);
        cookieSecond.setText("per second: "+s+" cookies");

        //END_OF_PREFERENCES_MAIN



        CookieSecond();
    }



    //OVERRIDE_METHODS

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        return super.onOptionsItemSelected(item);
    }

    //END_OF_OVERRIDE_METHODS



    //ALL_CLICKS_LISTENER

    private View.OnClickListener onClickListener=new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId())
            {
                case R.id.cookie:
                    onCookie();
                    break;
                case R.id.clean:
                    onClean();
                    break;
                case R.id.bShop:
                    closeShop();
                    break;
                case R.id.sShop:
                    startShop();
                    break;
            }
        }
    };

    //END_OF_ALL_CLICKS_LISTENER



    //ON_CLICK_EVENTS

    public void onCookie()
    {
        ccount=ccount.add(cclick);
        cookieCount.setText(ccount.toString()+" cookies");
        editor.putString(C_COUNT, ccount.toString());
        editor.commit();
        imageButton.startAnimation(animation);
    }



    public void onClean()
    {
        editor.putString(C_COUNT,"0");
        editor.apply();
        cookieCount.setText("0 cookies");
        ccount=new BigInteger("0");
    }



    public void CookieSecond()
    {
        countDownTimer.start();
    }



    public void closeShop()
    {
        sShop.setVisibility(View.VISIBLE);
        fShop.setVisibility(View.GONE);
    }



    public void startShop()
    {
        sShop.setVisibility(View.GONE);
        fShop.setVisibility(View.VISIBLE);
    }

    //END_OF_ON_CLICK_EVENTS
}
